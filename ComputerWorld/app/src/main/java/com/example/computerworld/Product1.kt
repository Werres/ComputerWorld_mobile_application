package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product1)

        val arrayAdapter: ArrayAdapter<*>

        val Product1 = arrayOf(

            "1. Накопитель SDD QUMO 60GB — 1890 руб.",
            "2. Накопитель SSD KINGSTON 240GB A400 — 3060 руб.",
            "3. Накопитель SSD KINGSTON 120GB A400 — 2680 руб.",
            "4. Жесткий диск 500GB 2.5″ 128MB SEAGATE MOBILE — 3640 руб.",
            "5. Жесткий диск 1000GB 2.5″ 8MB WD SCORPIO BLUE — 5440 руб.",
            "6. Жесткий диск 4000GB 3.5″ для видеонаблюдения — 18190 руб.",
            "7. Жесткий диск 500GB 3.5″ TOSHIBA 64MB  — 3780 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product1List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product1)

        mListView.adapter = arrayAdapter

    }
}