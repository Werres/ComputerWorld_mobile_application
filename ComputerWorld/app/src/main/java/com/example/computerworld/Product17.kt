package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product17 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product17)

        val arrayAdapter: ArrayAdapter<*>

        val Product17 = arrayOf(

            "1. Материнская плата AMD ASROCK 760GM-HDV AM3 — 4940 руб.",
            "2. Материнская плата AMD M/B MSI A68HM-E33 V2 USB3.0 RTL FM2+ — 4540 руб.",
            "3. Материнская плата INTEL M/B ASUS PRIME B360M-D RTL — 8790 руб.",
            "4. Материнская плата INTEL M/B ASUS PRIME H410R-SI WHITE BOX (S1200) — 7490 руб.",
            "5. Материнская плата INTEL M/B ASUS PRIME H410M-K — 7780 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product17List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product17)

        mListView.adapter = arrayAdapter
    }
}