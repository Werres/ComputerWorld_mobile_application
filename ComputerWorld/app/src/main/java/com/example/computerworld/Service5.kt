package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service5 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service5)

        val arrayAdapter: ArrayAdapter<*>

        val Service5 = arrayOf(

            "1. Настройка интернета прямого подключения — 300 руб.",
            "2. Настройка роутера/маршрутизатора (WIFI) — 500 руб.",
            "3. Обжатие сетевого провода — 50 руб.",
            "4. Подключение пк к локальной сети без прокладки кабеля — 500 руб.",
            "5. Подключение профессионального сетевого принтера — 2500 руб.",
            "6. Подключение сетевого принтера (1 ПК) — 500 руб.",
            "7. Установка серверного программного обеспечения — 2500 руб.",
            "8. Почасовая работа с сетевым оборудованием — от 500 руб.",
            "9. Наценка за срочность работ с сетевым оборудованием — 50%."

        )

        var mListView = findViewById<ListView>(R.id.Service5List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Service5)

        mListView.adapter = arrayAdapter
    }
}