package com.example.computerworld

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainService : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_service)

        val RepairSystemUnit: Button = findViewById (R.id.RepairSystemUnit)

        RepairSystemUnit.setOnClickListener {
            val intent = Intent(this, Service1::class.java)
            startActivity(intent)
        }

        val RepairLaptop: Button = findViewById (R.id.RepairLaptop)

        RepairLaptop.setOnClickListener {
            val intent = Intent(this, Service2::class.java)
            startActivity(intent)
        }

        val WorkingSoftware: Button = findViewById (R.id.WorkingSoftware)

        WorkingSoftware.setOnClickListener {
            val intent = Intent(this, Service3::class.java)
            startActivity(intent)
        }

        val RepairOfficeEquipment: Button = findViewById (R.id.RepairOfficeEquipment)

        RepairOfficeEquipment.setOnClickListener {
            val intent = Intent(this, Service4::class.java)
            startActivity(intent)
        }

        val ConfiguringNetworkEquipment: Button = findViewById (R.id.ConfiguringNetworkEquipment)

        ConfiguringNetworkEquipment.setOnClickListener {
            val intent = Intent(this, Service5::class.java)
            startActivity(intent)
        }

        val RepairRefillingCartridges: Button = findViewById (R.id.RepairRefillingCartridges)

        RepairRefillingCartridges.setOnClickListener {
            val intent = Intent(this, Service6::class.java)
            startActivity(intent)
        }

        val InstallationEquipment: Button = findViewById (R.id.InstallationEquipment)

        InstallationEquipment.setOnClickListener {
            val intent = Intent(this, Service7::class.java)
            startActivity(intent)
        }

        val Additionalservices: Button = findViewById (R.id.Additionalservices)

        Additionalservices.setOnClickListener {
            val intent = Intent(this, Service8::class.java)
            startActivity(intent)
        }
    }
}