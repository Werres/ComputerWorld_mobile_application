package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service6 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service6)

        val arrayAdapter: ArrayAdapter<*>

        val Service6 = arrayOf(

            "1. Заправка лазерного картриджа стандарт без материала — 280 руб.",
            "2. Заправка лазерного картриджа не стандарт без материала — от 280 руб.",
            "3. Заправка лазерного картриджа стандарт с материалом — 500 руб.",
            "4. Заправка струйного картриджа стандарт с материалом — 300 руб.",
            "5. Заправка струйного картриджа увеличенного объёма с материалом — 500 руб.",
            "6. Заправка струйного картриджа уменьшенного объёма с материалом — 200 руб.",
            "7. Профилактика лазерного картриджа стандарт — 280 руб.",
            "8. Срочная заправка лазерного картриджа (дополнительно к стоимости) — 200 руб."

        )

        var mListView = findViewById<ListView>(R.id.Service6List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Service6)

        mListView.adapter = arrayAdapter
    }
}