package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service4)

        val arrayAdapter: ArrayAdapter<*>

        val Service4 = arrayOf(

            "1. Дагностика оргтехники не стандарт (МФУ/принтер/копир) — 600 руб.",
            "2. Дагностика оргтехники стандарт (МФУ/принтер/копир) — 400 руб.",
            "3. Профилактика (МФУ/принтера/копира) — от 1000 руб.",
            "4. Замена ролика подачи бумаги без материала — от 1000 руб.",
            "5. Замена стекла сканера/МФУ без материала — 800 руб.",
            "6. Замена термопленки без материала — от 1 200 руб.",
            "7. Извлечение инородных предметов из МФУ/принтера/копира — 1000 руб.",
            "8. Очистка отстойника печатающей головки принтера/МФУ — 500 руб.",
            "9. Промывка печатающей головки струйного принтера/МФУ — от 800 руб.",
            "10. Профилактика — от 500 руб.",
            "11. Прошивка принтера — от 1 200 руб.",
            "12. Ремонт блока питания (МФУ/принтер/копир) — от 750 руб.",
            "13. Ремонт лазерного принтера (без стоимости запчастей) — от 1000 руб.",
            "14. Ремонт механики устройства (МФУ/принтер/копир) без материала — от 800 руб.",
            "15. Замена блока лазера, блока сканера (МФУ/принтер/копир) без материала — 1000 руб.",
            "16. Ремонт термозакрепительного узла без материала — от 900 руб.",
            "17. Замена тормозной площадки с материалом — от 400 руб.",
            "18. Наценка за срочность работ по ремонту оргтехники — 100%."

        )

        var mListView = findViewById<ListView>(R.id.Service4List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Service4)

        mListView.adapter = arrayAdapter
    }
}