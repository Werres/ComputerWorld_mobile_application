package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product18 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product18)

        val arrayAdapter: ArrayAdapter<*>

        val Product18 = arrayOf(

            "1. Микрофон DEFENDER MIC-130 — 270 руб.",
            "2. Микрофон SENNHEISER E 835 — 6990 руб.",
            "3. Микрофон SVEN MK-200 — 320 руб.",
            "4. Наушники DEFENDER HN-930 беспроводные — 690 руб.",
            "5. Наушники RITMIX RH-533USB — 890 руб.",
            "6. Наушники RITMIX RH-140 SPACER GREY — 280 руб.",
            "7. Наушники SVEN AP-940MV с микрофоном  — 1540 руб.",
            "8. Наушники SVEN AP-700 с микрофоном  — 620 руб.",
            "9. Наушники SVEN E-135  — 270 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product18List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product18)

        mListView.adapter = arrayAdapter
    }
}