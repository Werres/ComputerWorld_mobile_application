package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product8 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product8)

        val arrayAdapter: ArrayAdapter<*>

        val Product8 = arrayOf(

            "1. ПО WINDOWS VISTA HOME BASIC (OEI) — 3990 руб.",
            "2. Антивирус KASPERSKY INTERNET SECURITY 3-DEVICE 1 YEAR — 1820 руб.",
            "3. Антивирус АНТИВИРУС NOD32 — 1040 руб."
            )

        var mListView = findViewById<ListView>(R.id.Product8List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product8)

        mListView.adapter = arrayAdapter
    }
}