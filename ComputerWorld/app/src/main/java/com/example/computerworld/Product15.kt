package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product15 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product15)

        val arrayAdapter: ArrayAdapter<*>

        val Product15 = arrayOf(

            "1. Монитор Ж/К 21.5″ LG 22MK400A-B BLACK — 7460 руб.",
            "2. Монитор Ж/К 21,5″ PHILIPS 223V5LSB2/10(62) BLACK — 6120 руб.",
            "3. Монитор Ж/К 19.5″ PHILIPS 203V5LSB26 (10/62) — 7150 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product15List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product15)

        mListView.adapter = arrayAdapter
    }
}