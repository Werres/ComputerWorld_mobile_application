package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service3)

        val arrayAdapter: ArrayAdapter<*>

        val Service3 = arrayOf(

            "1. Администрирование серверного программного обеспечения(почасовая оплата) — от 1500 руб.",
            "2. Перенос информации — ОТ 500Р.",
            "3. Работа с программным обеспечением предоставленным заказчиком почасовая — от 1 200 руб.",
            "4. Профилактические работы с по (оптимизация) — 1000 руб.",
            "5. Восстановление информации с HDD — от 1 000 руб.",
            "6. Восстановление операционной системы — 800 руб.",
            "7. Клонирование жесткого диска — 800 руб.",
            "8. Установка драйверов устройств (всех) — 700 руб.",
            "9. Установка кодеков — 250 руб.",
            "10. Установка ОС (win 7, win 8, linux) производится при наличии лицензионного ключа у клиента — 700 руб.",
            "11. Удаление банера — 700 руб.",
            "12. Удаление вирусов — 1000 руб.",
            "13. Установка антивируса AVAST (1 год) — 700 руб.",
            "14. Установка антивируса касперского (1год) без стоимости антивируса — 400 руб.",
            "15. Установка антивируса касперского IS (1год) без стоимости антивируса — 400 руб.",
            "16. Установка драйвера — 250Р.",
            "17. Наценка за срочность работ с программным обеспечением — 100%."

        )

        var mListView = findViewById<ListView>(R.id.Service3List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Service3)

        mListView.adapter = arrayAdapter
    }
}