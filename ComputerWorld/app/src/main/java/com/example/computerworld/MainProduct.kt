package com.example.computerworld

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainProduct : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_product)

        val Harddisk: Button = findViewById (R.id.Harddisk)

        Harddisk.setOnClickListener {
            val intent = Intent(this, Product1::class.java)
            startActivity(intent)
        }

        val UPS: Button = findViewById (R.id.UPS)

        UPS.setOnClickListener {
            val intent = Intent(this, Product2::class.java)
            startActivity(intent)
        }

        val Keyboard: Button = findViewById (R.id.Keyboard)

        Keyboard.setOnClickListener {
            val intent = Intent(this, Product3::class.java)
            startActivity(intent)
        }

        val Speaker: Button = findViewById (R.id.Speaker)

        Speaker.setOnClickListener {
            val intent = Intent(this, Product4::class.java)
            startActivity(intent)
        }

        val Computer: Button = findViewById (R.id.Computer)

        Computer.setOnClickListener {
            val intent = Intent(this, Product5::class.java)
            startActivity(intent)
        }

        val Case: Button = findViewById (R.id.Case)

        Case.setOnClickListener {
            val intent = Intent(this, Product6::class.java)
            startActivity(intent)
        }

        val Cooler: Button = findViewById (R.id.Cooler)

        Cooler.setOnClickListener {
            val intent = Intent(this, Product7::class.java)
            startActivity(intent)
        }

        val Software: Button = findViewById (R.id.Software)

        Software.setOnClickListener {
            val intent = Intent(this, Product8::class.java)
            startActivity(intent)
        }

        val PrinterCopierScanner: Button = findViewById (R.id.PrinterCopierScanner)

        PrinterCopierScanner.setOnClickListener {
            val intent = Intent(this, Product9::class.java)
            startActivity(intent)
        }

        val Videocard: Button = findViewById (R.id.Videocard)

        Videocard.setOnClickListener {
            val intent = Intent(this, Product10::class.java)
            startActivity(intent)
        }

        val Powersupply: Button = findViewById (R.id.Powersupply)

        Powersupply.setOnClickListener {
            val intent = Intent(this, Product11::class.java)
            startActivity(intent)
        }

        val Soundcard: Button = findViewById (R.id.Soundcard)

        Soundcard.setOnClickListener {
            val intent = Intent(this, Product12::class.java)
            startActivity(intent)
        }

        val Alaptop: Button = findViewById (R.id.Alaptop)

        Alaptop.setOnClickListener {
            val intent = Intent(this, Product13::class.java)
            startActivity(intent)
        }

        val Processor: Button = findViewById (R.id.Processor)

        Processor.setOnClickListener {
            val intent = Intent(this, Product14::class.java)
            startActivity(intent)
        }

        val Monitor: Button = findViewById (R.id.Monitor)

        Monitor.setOnClickListener {
            val intent = Intent(this, Product15::class.java)
            startActivity(intent)
        }

        val Mouse: Button = findViewById (R.id.Mouse)

        Mouse.setOnClickListener {
            val intent = Intent(this, Product16::class.java)
            startActivity(intent)
        }

        val Motherboard: Button = findViewById (R.id.Motherboard)

        Motherboard.setOnClickListener {
            val intent = Intent(this, Product17::class.java)
            startActivity(intent)
        }

        val EarpieceMicrophone: Button = findViewById (R.id.EarpieceMicrophone)

        EarpieceMicrophone.setOnClickListener {
            val intent = Intent(this, Product18::class.java)
            startActivity(intent)
        }
    }
}