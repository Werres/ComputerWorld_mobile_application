package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product6 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product6)

        val arrayAdapter: ArrayAdapter<*>

        val Product6 = arrayOf(

            "1. Корпус SUPERPOWER MIDITOWER SP WINARD 3010 с 450W USB BLACK-SILVER — 3290 руб.",
            "2. Корпус SUPERPOWER MIDITOWER SP WINARD 3067 без БП — 2240 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product6List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product6)

        mListView.adapter = arrayAdapter
    }
}