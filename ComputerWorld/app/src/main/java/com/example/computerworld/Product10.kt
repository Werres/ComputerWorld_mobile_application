package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product10 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product10)

        val arrayAdapter: ArrayAdapter<*>

        val Product10 = arrayOf(

            "1. Видеокарта ASUS RADEON R7 250 2GB DDR5 — 8540 руб.",
            "2. Видеокарта PCI-E 2048MB GT1030 PALIT 64BIT DDR5 OEM — 7990 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product10List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product10)

        mListView.adapter = arrayAdapter
    }
}