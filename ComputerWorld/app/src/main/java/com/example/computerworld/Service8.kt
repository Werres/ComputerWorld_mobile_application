package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service8 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service8)

        val arrayAdapter: ArrayAdapter<*>

        val Service8 = arrayOf(

            "1. Выезд технического специалиста по городу срочный — 1000 руб.",
            "2. Выезд технического специалиста по району срочный — от 1000 руб.",
            "3. Выезд технического специалиста по городу — 500 руб.",
            "4. Выезд технического специалиста по району — от 500 руб.",
            "5. Доставка по городу (легковой а/м) — 300 руб.",
            "6. Доставка по району (легковой а/м) — от 300 руб.",
            "7. Печать стандарт 1 лист — 10 руб.",
            "8. Почасовая работа — от 500 руб.",
            "9. Экспертиза технического состояния оргтехники 1 единица техники — 500 руб.",
            "10. Экспертиза технического состояния оргтехники более 1ой единицы техники — 300 руб.",
            "11. Срочная экспертиза технического состояния оргтехники 1 единица техники — 750 руб."

        )

        var mListView = findViewById<ListView>(R.id.Service8List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Service8)

        mListView.adapter = arrayAdapter
    }
}