package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service1)

        val arrayAdapter: ArrayAdapter<*>

        val Service1 = arrayOf(

            "1. Замена батарейки BIOS (с учетом стоимости батарейки) — 250 руб.",
            "2. Замена блока питания (без стоимости блока питания) — от 350 руб.",
            "3. Замена видеокарты (без стоимости видеокарты) — 350 руб.",
            "4. Замена жесткого диска (без стоимости жесткого диска) — 350 руб.",
            "5. Замена кулера (без стоимости кулера) — ОТ 500 руб.",
            "6. Замена материнской платы (без стоимости материнской платы) — ОТ 1000 руб.",
            "7. Замена CD/DVD привода (без стоимости привода) — 250 руб.",
            "8. Замена провода питания 6PIN/4 PIN (без стоимости провода) — 100 руб.",
            "9. Обновление BIOS материнской платы — 500 руб.",
            "10. Ремонт материнской платы (без стоимости комплектующих и з/частей) — ОТ 1 000 руб.",
            "11. Установка ОЗУ (без стоимости ОЗУ) — 250 руб.",
            "12. Профилактика И техническое обслуживание системного блока (с заменой термопасты) — 1500 руб.",
            "13. Наценка за срочность работ по ремонту системного блока — 100%."

            )

        var mListView = findViewById<ListView>(R.id.Service1List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Service1)

        mListView.adapter = arrayAdapter
    }
}