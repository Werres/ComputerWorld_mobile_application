package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product4)

        val arrayAdapter: ArrayAdapter<*>

        val Product4 = arrayOf(

            "1. Колонки DEFENDER MERCURY 35(MK II) 2.0 (2*18ВТ) — 4820,00 руб.",
            "2. Колонки DIALOG COLIBRI AC-02UP B-W 2.0 2*2,5W USB— 390,00 руб.",
            "3. Колонки DIALOG AST-25UP BK 2.0 6W USB — 1440,00 руб.",
            "4. Колонки GEMBIRD SPK-201 2.0, USB — 690,00 руб.",
            "5. Колонки GEMBIRD SPK-100W 2.0, USB — 680,00 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product4List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product4)

        mListView.adapter = arrayAdapter
    }
}