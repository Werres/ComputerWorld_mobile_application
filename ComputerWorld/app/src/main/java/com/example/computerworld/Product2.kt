package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product2)

        val arrayAdapter: ArrayAdapter<*>

        val Product2 = arrayOf(

            "1. Аккумулятор для ИБП SVEN SV1290 (9А/Ч, 12В) — 1790 руб.",
            "2. Аккумулятор для ИБП SVEN SV1272 (7.2А/Ч, 12В) — 1480 руб.",
            "3. Аккумулятор для ИБП SVEN SV12120 (12А/Ч, 12В) — 2090 руб.",
            "4. Аккумулятор для ИБП SVEN SV1213S (1.3А/Ч, 12В) — 640 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product2List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product2)

        mListView.adapter = arrayAdapter
    }
}