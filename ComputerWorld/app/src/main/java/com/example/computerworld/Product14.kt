package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product14 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product14)

        val arrayAdapter: ArrayAdapter<*>

        val Product14 = arrayOf(

            "1. Процессор CPU AMD A4 6300K (OEM) 3.7ГГЦ FM2 — 1990 руб.",
            "2. Процессор CPU INTEL CORE I3-10100F — 11640 руб.",
            "3. Процессор CPU INTEL PENTIUM G4400 3.3 ГГЦ (1151) OEM — 7290 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product14List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product14)

        mListView.adapter = arrayAdapter
    }
}