package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product13 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product13)

        val arrayAdapter: ArrayAdapter<*>

        val Product13 = arrayOf(

            "1. Ноутбук HP 250 G7 15.6″ HD I3-7020U/8GB/256GB SSD/DOS — 36990 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product13List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product13)

        mListView.adapter = arrayAdapter
    }
}