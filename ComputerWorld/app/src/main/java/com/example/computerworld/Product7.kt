package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product7 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product7)

        val arrayAdapter: ArrayAdapter<*>

        val Product7 = arrayOf(

            "1. Кулер AMD IGLOO A360CU LIGHT(E) AM2/AM2+/AM3/FM1 — 790 руб.",
            "2. Кулер INTEL ICEHAMMER IH-3075WV FOR SOCKET 775/AM2 — 590 руб.",
            "3. Кулер COOLER INTEL ORIGINAL S1156 (AL) — 420 руб.",
            "4. Кулер для видео карты VC-RE GEMBIRD — 760 руб.",
            "5. Кулер для корпусов ZALMAN ZM-F4 — 1240 руб.",
            "6. Кулер для корпуса 92X92X25MM SLEEVE — 220 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product7List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product7)

        mListView.adapter = arrayAdapter
    }
}