package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product9 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product9)

        val arrayAdapter: ArrayAdapter<*>

        val Product9 = arrayOf(

            "1. МФУ PANASONIC KX-MB2110RUW — 13950 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product9List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product9)

        mListView.adapter = arrayAdapter
    }
}