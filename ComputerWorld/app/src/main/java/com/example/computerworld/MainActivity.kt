package com.example.computerworld

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val BtnProduct: Button = findViewById (R.id.BtnProduct)

        BtnProduct.setOnClickListener {
            val intent = Intent(this, MainProduct::class.java)
            startActivity(intent)
        }

        val BtnService: Button = findViewById (R.id.BtnService)

        BtnService.setOnClickListener {
            val intent = Intent(this, MainService::class.java)
            startActivity(intent)
        }

        val BtnAboutUs: Button = findViewById (R.id.BtnAbountUs)

        BtnAboutUs.setOnClickListener {
            val intent = Intent(this, MainAboutUs::class.java)
            startActivity(intent)
        }
    }
}