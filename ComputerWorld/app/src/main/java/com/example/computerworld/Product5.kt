package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product5 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product5)

        val arrayAdapter: ArrayAdapter<*>

        val Product5 = arrayOf(

            "1. Компьютер С494142Ц-INTEL PENTIUM G4400 / H110M PRO-VD / 4GB — 24990 руб.",
            "2. Компьютер С510108Ц-INTEL CORE I5 7400 / H110M PRO VD/ 8GB — 36990 руб.",
            "3. Компьютер ACER VERITON EX2620G [J4005/4GB/128GB SSD/LINUX] — 18980 руб.",
            "4. Компьютер C632743Ц NL-INTEL CORE I3-10100/8GB/SSD240GB/DOS — 35980 руб.",
            "5. Компьютер C649589Ц NL-INTEL CORE I5-10400/8GB/SSD240GB/DOS — 39840 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product5List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product5)

        mListView.adapter = arrayAdapter
    }
}