package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service7 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service7)

        val arrayAdapter: ArrayAdapter<*>

        val Service7 = arrayOf(

            "1. Установка оборудования без ПО — 100 руб.",
            "2. Установка оборудования с ПО — 300 руб.",
            "3. Установка роутера/маршрутизатора (WIFI) — 500 руб.",
            "4. Установка принтера/МФУ/копира стандарт — 300 руб.",
            "5. Установка принтера/МФУ/копира профессионального — 2500 руб.",
            "6. Установка рабочей станции — 300 руб."

        )

        var mListView = findViewById<ListView>(R.id.Service7List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Service7)

        mListView.adapter = arrayAdapter
    }
}