package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product16 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product16)

        val arrayAdapter: ArrayAdapter<*>

        val Product16 = arrayOf(

            "1. Мышь DEFENDER OPTIMUM (MM-) 140 USB S — 250 руб.",
            "2. Мышь DEFENDER GM-090L SKY DRAGON ПРОВОДНАЯ USB ИГРОВАЯ — 540 руб.",
            "3. Мышь GENIUS MINI NAVIGATOR USB SILVER БЕСПРОВОДНАЯ ОПТ. — 690 руб.",
            "4. Мышь GENIUS TRAVELER 525TC LASER USB ЛАЗЕР. ПРОВОДНАЯ — 690 руб.",
            "5. Мышь OKLICK 745G — 980 руб.",
            "6. Жесткий диск 4000GB 3.5″ для видеонаблюдения — 18190 руб.",
            "7. Жесткий диск 500GB 3.5″ TOSHIBA 64MB  — 3780 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product16List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product16)

        mListView.adapter = arrayAdapter
    }
}