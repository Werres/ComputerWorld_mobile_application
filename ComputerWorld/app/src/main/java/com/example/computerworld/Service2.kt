package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service2)

        val arrayAdapter: ArrayAdapter<*>

        val Service2 = arrayOf(

            "1. Диагностика (определение основной неисправности) — 500 руб.",
            "2. Диагностика (определение основной неисправности) — 500 руб.",
            "3. Диагностика (полная диагностика устройства с подготовкой диагностической карты ) — 1000 руб.",
            "4. Диагностика (полная диагностика устройства с подготовкой диагностической карты ) — 1000 руб.",
            "5. Разборка/Сборка ноутбука — 700 руб.",
            "6. Замена клавиатуры ноутбука (без стоимости клавиатуры) — от 500 руб.",
            "7. Замена кулера (без стоимости кулера)  — 1500 руб.",
            "8. Замена матрицы ноутбука (без стоимости матрицы) — 2000 руб.",
            "9. Замена жесткого диска (без стоимости жесткого диска) — от 300 руб.",
            "10. Замена CD/DVD привода (без стоимости привода) — от 300 руб.",
            "11. Клонирование жесткого диска — 1000 руб.",
            "12. Пайка разъёмов — ОТ 1 000 руб.",
            "13. Ремонт материнской платы ноутбука (без стоимости комплектующих и з/частей) — от 2 000 руб.",
            "14. Установка ОЗУ (без стоимости ОЗУ) — от 300 руб.",
            "15. Профилактика и техническое обслуживание ноутбука (с заменой термопасты) — от 1 500 руб.",
            "16. Наценка за срочность работ по ремонту ноутбука — 100%."

            )

        var mListView = findViewById<ListView>(R.id.Service2List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Service2)

        mListView.adapter = arrayAdapter
    }
}