package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product12 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product12)

        val arrayAdapter: ArrayAdapter<*>

        val Product12 = arrayOf(

            "1. Звуковая карта SB C-MEDIA 8738 4CHANNEL — 780 руб.",
            "2. Звуковая карта TRAA71 (C-MEDIA CM108) USB 2.0 — 640 руб.",
            "3. Звуковая карта TRUA3D (C-MEDIA CM108) USB 2.0 — 570 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product12List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product12)

        mListView.adapter = arrayAdapter
    }
}