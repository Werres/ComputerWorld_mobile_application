package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product11 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product11)

        val arrayAdapter: ArrayAdapter<*>

        val Product11 = arrayOf(

            "1. Блок питания ATX 500W AEROCOOL — 2480 руб.",
            "2. Блок питания ATX 600W EXEGATE — 3120 руб.",
            "3. Блок питания ATX 700W AEROCOOL VX SERIES — 4120 руб.",
            "4. Блок питания ATX 450W ACCORD — 1990 руб.",
            "5. Блок питания ATX 500W CHIEFTEC — 3890 руб.",
            "6. Блок питания ATX 500W ACCORD — 2790 руб.",
            "7. Блок питания ATX 500W INWIN  — 2380 руб.",
            "8. Блок питания ATX 800W AEROCOLL  — 4790 руб."


            )

        var mListView = findViewById<ListView>(R.id.Product11List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product11)

        mListView.adapter = arrayAdapter
    }
}