package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Product3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product3)

        val arrayAdapter: ArrayAdapter<*>

        val Product3 = arrayOf(

            "1. Клавиатура A4 Q135 BLOODY NEON USB — 1680 руб.",
            "2. Клавиатура A4 KR-85 проводная USB — 890 руб.",
            "3. Клавиатура DEFENDER CHIMERA GK-280DL — 1520 руб.",
            "4. Клавиатура DEFENDER ELEMENT HB-520 , USB(черный) — 390 руб.",
            "5. Клавиатура DIALOG KS-030U проводная, черная, USB — 390 руб.",
            "6. Клавиатура GENIUS SLIM STAR 335 USB, игровая — 2380 руб.",
            "7. Клавиатура LOGITECH K120 CLASSIC USB  — 790 руб.",
            "8. Клавиатура OKLICK 505 WHITE, SLIM  — 620 руб."

            )

        var mListView = findViewById<ListView>(R.id.Product3List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, Product3)

        mListView.adapter = arrayAdapter
    }
}